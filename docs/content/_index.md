+++
+++

## Overview

`rust-plus` 是我个人的 `rust` 学习追踪项目。

## Notes

- 🥣 {{ tlink(name="常识篇", url="/tags/general-knowledge") }}
- 🏜 {{ tlink(name="设计篇", url="/tags/system-design") }}

## Algorithms

- 🔗 链表
  - [单链表](https://gitlab.com/wavty/rust-plus/-/tree/main/src/single_linked_list)
- 🐞 [动态规划](https://gitlab.com/wavty/rust-plus/-/tree/main/src/dynamic_programming)
- 🇵🇪 [排列组合](https://gitlab.com/wavty/rust-plus/-/tree/main/src/permutation)
- 🧗 [二叉树](https://gitlab.com/wavty/rust-plus/-/tree/main/src/binary_tree)

## Online Presence

- Email: [mtaobo@gmail.com](mtaobo@gmail.com)
