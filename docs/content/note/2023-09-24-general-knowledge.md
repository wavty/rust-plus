+++
title = "常识篇-基础数据类型"
[taxonomies]
  tags = ["general-knowledge"]
+++

## 1. Rust 中变量、常量和静态变量的定义

下表展示了 Rust 中变量、常量和静态变量的定义、说明以及举例使用：

| 类型     | 定义                         | 说明                                                 | 举例                         |
| -------- | ---------------------------- | ---------------------------------------------------- | ---------------------------- |
| 变量     | `let x = 1;`                 | 声明并初始化一个变量 `x`，类型会自动推导             | `let x = 1;`                 |
| 常量     | `const MAX: i32 = 100;`      | 定义一个值不可变的常量 `MAX`，必须显式指定类型       | `const MAX: i32 = 100;`      |
| 静态变量 | `static mut COUNT: i32 = 0;` | 定义一个全局的可变静态变量 `COUNT`，必须显式指定类型 | `static mut COUNT: i32 = 0;` |

需要注意的是，在 Rust 中，变量和常量都是默认不可变的，如果需要可变的变量，可以使用 `mut` 关键字声明。而静态变量则需要使用 `static` 关键字声明，并且在声明时必须显式指定类型。

另外，由于静态变量是全局变量，因此在多线程程序中访问时需要考虑线程安全问题。如果需要在多个线程中访问静态变量，可以使用 `Arc`（原子引用计数）类型来实现。

## 2. rust 中有 void 类型吗？

Rust 中没有 `void` 类型。相反，Rust 使用 `()` 表示无返回值函数的返回类型，表示返回一个空元组。这种设计使得 Rust 中的函数类型更加清晰和精确，也更符合 Rust 的表达式风格。另外，Rust 中也没有空指针或空引用，取而代之的是 `Option` 枚举类型。

## 3. 简单讲述一下 rust 里面的 unit 类型

在 Rust 中，`()` 被称为 unit 类型，它只有一个值，也就是空元组 `()`。和其他语言中的 `void` 类型不同，unit 类型不是一个空指针或者空引用，而是一个真正的类型，它表示不包含有用信息的空值。

在 Rust 中，函数可以选择返回值，也可以选择不返回值。如果函数不需要返回值，那么就可以显式声明返回值类型为 `()`，表示返回空元组。例如，下面的代码定义了一个没有返回值的函数 `hello`：

```rust
fn hello() -> () {
    println!("Hello, world!");
}
```

另一方面，如果函数返回的是空指针或空引用，那么在 Rust 中可以使用 `Option` 类型来表示。`Option` 类型是一个枚举类型，它可以是 `Some(T)`（包含一个 `T` 类型的值）或 `None`（表示空值）。

总的来说，Rust 选择使用 `()` 作为空值类型，而不是像其他语言一样使用 `void`，这是因为 `()` 是一个真正的类型，有更多的可操作性和灵活性，而且更符合 Rust 的安全和表达式风格。

## 4. 举个使用 Option 类型的例子

```rust
fn fig(is_number: bool) -> Option<i32> {
    if is_number {
        Some(10)
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fig() {
        assert_eq!(fig(true), Some(10));
        assert_eq!(fig(false), None);
    }
}
```

这个代码示例定义了一个函数 `fig`，接受一个 `bool` 类型的参数 `is_number`，当 `is_number` 为 `true` 时，返回一个具体的 `Option<i32>` 类型的值，否则返回 `None`。

同时，代码示例中还定义了一个测试模块，包含了一个测试函数 `test_fig`，测试了 `fig` 函数的正确性。在测试函数中使用了 `assert_eq!` 宏来断言函数的输出是否符合预期。

## 5. 空结构体、元组结构体、普通结构体的定义与区别

在 Rust 中，有三种常见的结构体定义：空结构体、元组结构体和普通结构体。

### 空结构体

空结构体是指没有任何成员的结构体，定义语法如下：

```rust
struct EmptyStruct;
```

空结构体通常用于作为占位符，或者是实现某些 trait 的标记。例如，下面的例子定义了一个标记 trait `MyTrait`，实现了该 trait 的类型不需要具备任何状态：

```rust
trait MyTrait {}

struct MyType;

impl MyTrait for MyType {}

struct EmptyStruct;

impl MyTrait for EmptyStruct {}
```

### 元组结构体

元组结构体是一种特殊的结构体，它没有具名的成员，而是使用元组来表示其字段。元组结构体的定义语法如下：

```rust
struct TupleStruct(i32, f64);
```

元组结构体常常用于表示一组相关的值。例如，下面的例子定义了一个元组结构体 `Point`，表示二维平面上的一个点：

```rust
struct Point(f64, f64);

let p = Point(1.0, 2.0);
```

### 普通结构体

普通结构体是一种最常用的结构体，它由一个或多个具名的字段组成。定义语法如下：

```rust
struct MyStruct {
    field1: u32,
    field2: String,
    // ...
}
```

普通结构体通常用于表示一组具有不同类型的数据。例如，下面的例子定义了一个结构体 `Person`，表示一个人的基本信息：

```rust
struct Person {
    name: String,
    age: u32,
    gender: Gender,
}

enum Gender {
    Male,
    Female,
}

let person = Person {
    name: "Alice".to_string(),
    age: 30,
    gender: Gender::Female,
};
```

在上面的例子中，`Person`结构体由三个字段组成，分别是`name`、`age`和`gender`。每个字段都有自己的类型，而结构体类型则由这些字段的类型组成。

总的来说，不同类型的结构体适用于不同的场景。空结构体通常用于作为占位符，元组结构体适用于表示一组相关的值，而普通结构体适用于表示具有不同类型的数据。
