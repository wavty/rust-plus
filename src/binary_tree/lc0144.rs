use crate::{Solution, TreeNode};
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn preorder_traversal(root: Option<Rc<RefCell<TreeNode>>>) -> Vec<i32> {
        fn helper(root: Option<Rc<RefCell<TreeNode>>>, ans: &mut Vec<i32>) {
            if let Some(node) = root {
                ans.push(node.borrow().val);
                helper(node.borrow().left.clone(), ans);
                helper(node.borrow().right.clone(), ans);
            }
        }
        let mut ans = vec![];
        helper(root, &mut ans);
        ans
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_preorder_traversal() {
        let tree = Some(Rc::new(RefCell::new(TreeNode {
            val: 1,
            left: Some(Rc::new(RefCell::new(TreeNode::new(2)))),
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 3,
                left: Some(Rc::new(RefCell::new(TreeNode::new(4)))),
                right: None,
            }))),
        })));
        assert_eq!(Solution::preorder_traversal(tree), vec![1, 2, 3, 4]);
    }
}
