use crate::{Solution, TreeNode};
use std::cell::RefCell;
use std::rc::Rc;

impl Solution {
    pub fn generate_trees(n: i32) -> Vec<Option<Rc<RefCell<TreeNode>>>> {
        fn dfs(l: i32, r: i32) -> Vec<Option<Rc<RefCell<TreeNode>>>> {
            let mut ans = vec![];
            if l > r {
                ans.push(None);
                return ans;
            }
            for i in l..=r {
                let left_trees = dfs(l, i - 1);
                let right_trees = dfs(i + 1, r);
                for lnode in left_trees.iter() {
                    for rnode in right_trees.iter() {
                        let mut root = Some(Rc::new(RefCell::new(TreeNode::new(i))));
                        root.as_mut().unwrap().borrow_mut().left = lnode.clone();
                        root.as_mut().unwrap().borrow_mut().right = rnode.clone();
                        ans.push(root);
                    }
                }
            }
            ans
        }
        dfs(1, n)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_generate_trees() {
        assert_eq!(Solution::generate_trees(1).len(), 1);
        assert_eq!(Solution::generate_trees(3).len(), 5);
    }
}
