use crate::Solution;

impl Solution {
    pub fn get_skyline(buildings: Vec<Vec<i32>>) -> Vec<Vec<i32>> {
        let mut skys: Vec<Vec<i32>> = vec![];
        let mut lines = vec![];
        for building in buildings.iter() {
            lines.push(building[0]);
            lines.push(building[1]);
        }
        lines.sort_unstable();
        let mut pq = std::collections::BinaryHeap::new();
        let (mut city, n) = (0, buildings.len());
        for line in lines {
            while city < n && buildings[city][0] <= line && buildings[city][1] > line {
                pq.push((buildings[city][2], buildings[city][1]));
                city += 1;
            }
            while !pq.is_empty() && pq.peek().unwrap().1 <= line {
                pq.pop();
            }
            let mut high = 0;
            if !pq.is_empty() {
                high = pq.peek().unwrap().0;
            }
            if !skys.is_empty() && skys.last().unwrap()[1] == high {
                continue;
            }
            skys.push(vec![line, high]);
        }
        skys
    }
}

#[cfg(test)]
mod test {
    use std::collections::BinaryHeap;

    #[test]
    fn test() {
        // 创建最小堆，元组的第一个元素（i32）决定优先级
        let mut min_heap: BinaryHeap<(i32, i32)> = BinaryHeap::new();

        min_heap.push((5, 1));
        min_heap.push((2, 2));
        min_heap.push((9, 3));

        while let Some((key, value)) = min_heap.pop() {
            println!("Min-Heap: Key={}, Value={}", key, value);
        }

        // 创建最大堆，通过元组的第一个元素取负数来实现
        let mut max_heap: BinaryHeap<(i32, i32)> = BinaryHeap::new();

        max_heap.push((-5, 1));
        max_heap.push((-2, 2));
        max_heap.push((-9, 3));

        while let Some((key, value)) = max_heap.pop() {
            println!("Max-Heap: Key={}, Value={}", -key, value);
        }
    }
}
