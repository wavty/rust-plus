use crate::{ListNode, Solution};

impl Solution {
    pub fn delete_duplicates2(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        let mut dummy = Some(Box::new(ListNode::new(101)));
        let mut pre = dummy.as_mut().unwrap();
        let mut pre_val = 101;
        let mut head = head;
        while let Some(mut node) = head {
            head = node.next.take();
            let val = node.val;
            if pre_val != node.val && (head.is_none() || head.as_ref().unwrap().val != node.val) {
                pre.next = Some(node);
                pre = pre.next.as_mut().unwrap();
            }
            pre_val = val;
        }
        dummy.unwrap().next
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_delete_duplicates() {
        let mut head = Some(Box::new(ListNode::new(1)));
        let mut cur = head.as_mut().unwrap();
        for i in 2..=5 {
            cur.next = Some(Box::new(ListNode::new(i)));
            cur = cur.next.as_mut().unwrap();
            cur.next = Some(Box::new(ListNode::new(i)));
            cur = cur.next.as_mut().unwrap();
        }
        cur.next = Some(Box::new(ListNode::new(6)));
        cur = cur.next.as_mut().unwrap();
        cur.next = Some(Box::new(ListNode::new(7)));
        let result = Solution::delete_duplicates2(head);
        println!("{:?}", result);
        let expect_result = vec![1, 6, 7];
        let mut cur = &result;
        for i in expect_result {
            assert_eq!(cur.as_ref().unwrap().val, i);
            cur = &cur.as_ref().unwrap().next;
        }
    }
}
