use crate::{ListNode, Solution};

impl Solution {
    pub fn reverse_between(head: Option<Box<ListNode>>, left: i32, right: i32) -> Option<Box<ListNode>> {
        let mut dummy = Some(Box::new(ListNode { val: 0, next: head }));
        let mut pre = &mut dummy;
        for _ in 1..left {
            pre = &mut pre.as_mut().unwrap().next;
        }
        let mut cur = pre.as_mut().unwrap().next.take();
        for _ in 0..right - left + 1 {
            let next = cur.as_mut().unwrap().next.take();
            cur.as_mut().unwrap().next = pre.as_mut().unwrap().next.take();
            pre.as_mut().unwrap().next = cur.take();
            cur = next;
        }
        for _ in 0..right - left + 1 {
            pre = &mut pre.as_mut().unwrap().next;
        }
        pre.as_mut().unwrap().next = cur;
        dummy.unwrap().next
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_reverse_between() {
        let mut head = Some(Box::new(ListNode::new(1)));
        let mut cur = &mut head;
        for i in 2..=6 {
            cur.as_mut().unwrap().next = Some(Box::new(ListNode::new(i)));
            cur = &mut cur.as_mut().unwrap().next;
        }
        let result = Solution::reverse_between(head, 2, 4);
        println!("{:?}", result);
    }
}
