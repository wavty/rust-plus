use crate::{ListNode, Solution};

impl Solution {
    pub fn reverse_k_group(head: Option<Box<ListNode>>, k: i32) -> Option<Box<ListNode>> {
        fn reverse(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
            let mut head = head;
            let mut pre = None;
            while let Some(mut node) = head {
                head = node.next.take();
                node.next = pre.take();
                pre = Some(node);
            }
            pre
        }

        let mut dummy = Some(Box::new(ListNode::new(0)));
        let mut pre = &mut dummy;
        let mut cur = head;
        while cur.is_some() {
            let mut q = &mut cur;
            for _ in 0..k - 1 {
                if q.is_none() {
                    break;
                }
                q = &mut q.as_mut().unwrap().next;
            }
            if q.is_none() {
                pre.as_mut().unwrap().next = cur;
                return dummy.unwrap().next;
            }

            let b = q.as_mut().unwrap().next.take();
            pre.as_mut().unwrap().next = reverse(cur);
            while pre.is_some() && pre.as_mut().unwrap().next.is_some() {
                pre = &mut pre.as_mut().unwrap().next;
            }
            cur = b;
        }
        dummy.unwrap().next
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_reverse_k_group() {
        let mut head = Some(Box::new(ListNode::new(1)));
        let mut cur = &mut head;
        for i in 2..=5 {
            cur.as_mut().unwrap().next = Some(Box::new(ListNode::new(i)));
            cur = &mut cur.as_mut().unwrap().next;
        }
        let result = Solution::reverse_k_group(head, 2);
        let expect_result = vec![2, 1, 4, 3, 5];
        let mut cur = &result;
        for i in expect_result {
            assert_eq!(cur.as_ref().unwrap().val, i);
            cur = &cur.as_ref().unwrap().next;
        }

        let head = Some(Box::new(ListNode::new(1)));
        let result = Solution::reverse_k_group(head, 1);
        let expect_result = vec![1];
        let mut cur = &result;
        for i in expect_result {
            assert_eq!(cur.as_ref().unwrap().val, i);
            cur = &cur.as_ref().unwrap().next;
        }
    }
}
