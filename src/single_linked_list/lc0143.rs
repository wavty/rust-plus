use crate::{ListNode, Solution};

impl Solution {
    pub fn reorder_list(head: &mut Option<Box<ListNode>>) {
        let mut len = 1;
        let mut cur = &head.as_mut().unwrap().next;
        while cur.is_some() {
            len += 1;
            cur = &cur.as_ref().unwrap().next;
        }

        let mut cur = &mut head.as_mut().unwrap().next;
        for _ in 0..(len / 2 - 1) {
            cur = &mut cur.as_mut().unwrap().next;
        }

        let mut right = cur.take();
        let mut pre = None;
        while let Some(mut node) = right {
            right = node.next.take();
            node.next = pre;
            pre = Some(node);
        }

        let mut cur = head;
        while let Some(mut node) = pre {
            pre = node.next.take();
            let mut h_next = cur.as_mut().unwrap().next.take();
            if h_next.is_none() {
                h_next = pre;
                pre = None;
            }
            node.next = h_next;

            cur.as_mut().unwrap().next = Some(node);
            cur = &mut cur.as_mut().unwrap().next.as_mut().unwrap().next;
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test() {
        let mut head = ListNode::from_array(&[1, 2, 3, 4]);
        Solution::reorder_list(&mut head);
        let mut cur = &head;
        let mut arr = vec![];
        while let Some(node) = cur {
            arr.push(node.val);
            cur = &node.next;
        }
        assert_eq!(arr, vec![1, 4, 2, 3]);

        let mut head = ListNode::from_array(&[1, 2, 3, 4, 5]);
        Solution::reorder_list(&mut head);
        let mut cur = &head;
        let mut arr = vec![];
        while let Some(node) = cur {
            arr.push(node.val);
            cur = &node.next;
        }
        assert_eq!(arr, vec![1, 5, 2, 4, 3]);
    }
}
