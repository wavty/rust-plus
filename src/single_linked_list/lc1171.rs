use crate::{ListNode, Solution};

// Definition for singly-linked list.
// #[derive(PartialEq, Eq, Clone, Debug)]
// pub struct ListNode {
//   pub val: i32,
//   pub next: Option<Box<ListNode>>
// }
//
// impl ListNode {
//   #[inline]
//   fn new(val: i32) -> Self {
//     ListNode {
//       next: None,
//       val
//     }
//   }
// }
impl Solution {
    pub fn remove_zero_sum_sublists(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        let dummy = Some(Box::new(ListNode { val: 0, next: head }));
        let mut last = std::collections::HashMap::new();
        let mut sum = 0;
        let mut p = dummy.as_ref();
        while let Some(node) = p {
            sum += node.val;
            last.insert(sum, node);
            p = node.next.as_ref();
        }

        let mut p = Some(Box::new(ListNode::new(0)));
        let mut q = p.as_mut();
        let mut sum = 0;
        while let Some(cur) = q {
            sum += cur.val;
            if let Some(next) = last.get(&sum) {
                cur.next = next.next.clone();
            }
            q = cur.next.as_mut();
        }
        p.unwrap().next
    }
}

#[cfg(test)]
mod test {
    use crate::{ListNode, Solution};

    #[test]
    fn test() {
        let head = ListNode::from_array(&[1, 2, -3, 3, 1]);
        let res = Solution::remove_zero_sum_sublists(head);
        println!("{:?}", res);

        let head = ListNode::from_array(&[1, 2, 3, -3, 4]);
        let res = Solution::remove_zero_sum_sublists(head);
        println!("{:?}", res);

        let head = ListNode::from_array(&[1, 2, 3, -3, -2]);
        let res = Solution::remove_zero_sum_sublists(head);
        println!("{:?}", res);
    }
}
