use crate::{ListNode, Solution};

impl Solution {
    pub fn delete_duplicates(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        let mut dummy = Some(Box::new(ListNode::new(i32::MAX)));
        let mut p = &mut dummy;

        let mut current = head;
        while let Some(mut node) = current {
            current = node.next.take();
            if p.as_mut().unwrap().val != node.val {
                p.as_mut().unwrap().next = Some(node);
                p = &mut p.as_mut().unwrap().next;
            }
        }
        dummy.unwrap().next
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_delete_duplicates() {
        let mut head = Some(Box::new(ListNode::new(1)));
        let mut cur = &mut head;
        for i in 2..=5 {
            cur.as_mut().unwrap().next = Some(Box::new(ListNode::new(i)));
            cur = &mut cur.as_mut().unwrap().next;
            cur.as_mut().unwrap().next = Some(Box::new(ListNode::new(i)));
            cur = &mut cur.as_mut().unwrap().next;
        }
        let result = Solution::delete_duplicates(head);
        let expect_result = vec![1, 2, 3, 4, 5];
        let mut cur = &result;
        for i in expect_result {
            assert_eq!(cur.as_ref().unwrap().val, i);
            cur = &cur.as_ref().unwrap().next;
        }
    }
}
