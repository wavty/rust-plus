use crate::Solution;

impl Solution {
    pub fn count_numbers_with_unique_digits(n: i32) -> i32 {
        if n == 0 {
            return 1;
        } else if n == 1 {
            return 10;
        }
        let (mut ans, mut cur) = (10, 9);
        for i in 0..n - 1 {
            cur *= 9 - i;
            ans += cur;
        }
        ans
    }
}

#[cfg(test)]
mod test {
    use crate::Solution;

    #[test]
    fn test() {
        assert_eq!(Solution::count_numbers_with_unique_digits(0), 1);
        assert_eq!(Solution::count_numbers_with_unique_digits(1), 10);
        assert_eq!(Solution::count_numbers_with_unique_digits(2), 91);
        assert_eq!(Solution::count_numbers_with_unique_digits(3), 739);
        assert_eq!(Solution::count_numbers_with_unique_digits(4), 5275);
        assert_eq!(Solution::count_numbers_with_unique_digits(5), 32491);
        assert_eq!(Solution::count_numbers_with_unique_digits(6), 168571);
        assert_eq!(Solution::count_numbers_with_unique_digits(7), 712891);
        assert_eq!(Solution::count_numbers_with_unique_digits(8), 2345851);
        assert_eq!(Solution::count_numbers_with_unique_digits(9), 5611771);
        assert_eq!(Solution::count_numbers_with_unique_digits(10), 8877691);
        assert_eq!(Solution::count_numbers_with_unique_digits(11), 8877691);
    }
}
