use crate::Solution;

impl Solution {
    pub fn word_break(s: String, word_dict: Vec<String>) -> bool {
        let words: std::collections::HashSet<String> = word_dict.into_iter().collect();
        let mut dp = vec![false; s.len() + 1];
        dp[0] = true;
        for i in 1..=s.len() {
            for j in 0..i {
                dp[i] |= dp[j] && words.contains(&s[j..i]);
            }
        }
        dp[s.len()]
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_word_break() {
        assert_eq!(Solution::word_break("leetcode".to_string(), vec!["leet".to_string(), "code".to_string()]), true);
        assert_eq!(Solution::word_break("applepenapple".to_string(), vec!["apple".to_string(), "pen".to_string()]), true);
        assert_eq!(Solution::word_break("catsandog".to_string(), vec!["cats".to_string(), "dog".to_string(), "sand".to_string(), "and".to_string(), "cat".to_string()]), false);
    }
}
